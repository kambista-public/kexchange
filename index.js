'use strict'

/**
 * Method for do an exchange with a given tc
 *
 * @param {Object} tc object with ask and bid to use into of exchanges calcs
 * @param {Number} amount amount of money desire to exchange
 * @param {String} active active value to exchange: S or R
 * @param {String} originCurrency the currency code to represented for the amountSent coin
 * @param {String} destinationCurrency the currency code for destination kind of coin
 * @return {Object} An object with data result from calcs
 */
const calculate = (tc, amount, active, originCurrency, destinationCurrency) => {
  let payload = {}
  if (!['S', 'R'].includes(active)) throw new Error('Action kexchange code')
  switch (originCurrency) {
    case 'PEN':
      if (destinationCurrency === 'USD') {
        payload.rate = tc.ask
        payload.exchange = parseFloat(
          (active === 'S' ? amount / tc.ask : amount * tc.ask).toFixed(2)
        )
        payload.tc = tc
        return payload
      }
    case 'ARS':
      if (destinationCurrency === 'USD') {
        payload.rate = tc.ask
        payload.exchange = parseFloat(
          (active === 'S' ? amount / tc.ask : amount * tc.ask).toFixed(2)
        )
        payload.tc = tc
        return payload
      }
    case 'USD':
      if (destinationCurrency === 'PEN' || destinationCurrency === 'ARS') {
        payload.rate = tc.bid
        payload.exchange = parseFloat(
          (active === 'S' ? amount * tc.bid : amount / tc.bid).toFixed(2)
        )
        payload.tc = tc
        return payload
      }
    default:
      throw new Error('Combinacion de monedas invalidas.')
  }
}

module.exports = {
  calculate
}

// Allow use of default import syntax in TypeScript
module.exports.default = calculate

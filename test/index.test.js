const kexchange = require('./../index')

test('PEN=>USD(SALE): Send 550 ask=3.359 bid=3.324 ', () => {
  const sendTc = {
    ask: 3.359,
    bid: 3.324
  }
  const { exchange, tc } = kexchange.calculate(sendTc, 550, 'S', 'PEN', 'USD')
  expect(exchange).toBe(163.74)
  expect(sendTc).toBe(tc)
})

test('USD=>PEN(BUY): Send 550 ask=3.359 bid=3.324 ', () => {
  const tc = {
    ask: 3.359,
    bid: 3.324
  }
  const { exchange } = kexchange.calculate(tc, 550, 'S', 'USD', 'PEN')
  expect(exchange).toBe(1828.2)
})

test('PEN=>USD(SALE): Receive 163.74 ask=3.359 bid=3.324 ', () => {
  const sendTc = {
    ask: 3.359,
    bid: 3.324
  }
  const { exchange, tc } = kexchange.calculate(sendTc, 163.74, 'R', 'PEN', 'USD')
  expect(exchange).toBe(550)
  expect(sendTc).toBe(tc)
})

test('USD=>PEN(BUY): Receive 1828.20 ask=3.359 bid=3.324 ', () => {
  const tc = {
    ask: 3.359,
    bid: 3.324
  }
  const { exchange } = kexchange.calculate(tc, 1828.2, 'R', 'USD', 'PEN')
  expect(exchange).toBe(550)
})

test('Invalid active code', () => {
  const sendTc = {
    ask: 3.359,
    bid: 3.324
  }
  expect(() => {
    kexchange.calculate(sendTc, 550, 'H', 'PEN', 'USD')
  }).toThrow()
})
